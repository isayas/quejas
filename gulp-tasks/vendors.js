'use strict'
var gulp = require('gulp');
var concat = require('gulp-concat');
var merge = require('merge-stream');
const del = require('del');


gulp.task('clean:vendors', function () {
    return del([
      './assets/vendors/**/*'
    ]);
});

/*Building vendor scripts needed for basic template rendering*/
gulp.task('buildBaseVendorScripts', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/popper.js/dist/umd/popper.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.min.js',
        './node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js',
        './node_modules/sweetalert2/dist/sweetalert2.all.min.js',
        './node_modules/jquery-steps/build/jquery.steps.min.js',
        './node_modules/jszip/dist/jszip.min.js',
        './node_modules/pdfmake/build/pdfmake.min.js',
        './node_modules/datatables.net/js/jquery.dataTables.min.js',
        './node_modules/datatables.net-buttons/js/dataTables.buttons.min.js',
        './node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
        './node_modules/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js',
        './node_modules/datatables.net-buttons/js/buttons.html5.min.js',
        './node_modules/datatables.net-buttons/js/buttons.print.min.js'
    ])
      .pipe(concat('vendor.bundle.base.js'))
      .pipe(gulp.dest('./assets/vendors/js'));
});

/*Building vendor styles needed for basic template rendering*/
gulp.task('buildBaseVendorStyles', function() {
    return gulp.src(['./node_modules/perfect-scrollbar/css/perfect-scrollbar.css',
'./node_modules/sweetalert2/dist/sweetalert2.min.css',
'./node_modules/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css'
        ])
      .pipe(concat('vendor.bundle.base.css'))
      .pipe(gulp.dest('./assets/vendors/css'));
});

/*Scripts for addons*/
gulp.task('buildOptionalVendorScripts', function() {
    var aScript1 = gulp.src(['node_modules/chart.js/dist/Chart.min.js'])
        .pipe(gulp.dest('./assets/vendors/chart.js'));
    return merge(aScript1);
});


/*Styles for addons*/
gulp.task('buildOptionalVendorStyles', function() {
    var aStyle1 = gulp.src(['./node_modules/@mdi/font/css/materialdesignicons.min.css'])
        .pipe(gulp.dest('./assets/vendors/mdi/css'));
    var aStyle2 = gulp.src(['./node_modules/@mdi/font/fonts/*'])
        .pipe(gulp.dest('./assets/vendors/mdi/fonts'));
    return merge(aStyle1, aStyle2);
});

//Copy essential map files
gulp.task('copyMapFiles', function() {
    var map1 = gulp.src('node_modules/bootstrap/dist/js/bootstrap.min.js.map',
        './node_modules/pdfmake/build/pdfmake.min.js.map')
        .pipe(gulp.dest('./assets/vendors/js'));
    var map2 = gulp.src('node_modules/@mdi/font/css/materialdesignicons.min.css.map')
        .pipe(gulp.dest('./assets/vendors/mdi/css'));
    return merge(map1, map2);
});

/*sequence for building vendor scripts and styles*/
gulp.task('bundleVendors', gulp.series('clean:vendors', 'buildBaseVendorStyles','buildBaseVendorScripts', 'buildOptionalVendorStyles', 'buildOptionalVendorScripts', 'copyMapFiles'));