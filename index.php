<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>IEQROO | SIQyD</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />
  </head>
  <body>
	<!-- plugins:js -->
    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->

<?php
require_once 'control/connection.php';

$act = isset($_GET['action']) ? $_GET['action'] : '';

if ($auth->isLoggedIn()) {
	switch ($act) {
		case 'register':
			include 'view/v_register.inc';
			break;

		case 'report':
			include 'view/v_report.inc';
			break;

		case 'logout':
			$auth->logOut();
			$auth->destroySession();
			include 'view/v_login.inc';
			break;

		default:
			include 'view/v_dashboard.inc';
			break;
	}
}
else {
	switch ($act) {
		case 'login':
			try {
			    $auth->login($_POST['email'], $_POST['password'], 1);
			    include 'view/v_dashboard.inc';
			}
			catch (\Delight\Auth\InvalidEmailException $e) {
			    // wrong email address
			    echo "<script>Swal.fire('Incorrecto','correo electrónico','error');</script>";
			    include 'view/v_login.inc';

			}
			catch (\Delight\Auth\InvalidPasswordException $e) {
			    // wrong password
			    echo "<script>Swal.fire('Incorrecta','contraseña','error');</script>";
			    include 'view/v_login.inc';

			}
			catch (\Delight\Auth\EmailNotVerifiedException $e) {
			    // email not verified
			    echo "<script>Swal.fire('Falta','Verificar tu correo electrónico','warning');</script>";
			    include 'view/v_login.inc';

			}
			catch (\Delight\Auth\TooManyRequestsException $e) {
			    // too many requests
			    echo "<script>Swal.fire('Aviso','Se ha excedido el número de solicitudes','warning');</script>";
			    include 'view/login.inc';
			}
			break;
		default:
			include 'view/v_login.inc';
			break;
	}
}

?>

	<!-- inject:js -->
    <script src="assets/js/off-canvas.js"></script>
    <script src="assets/js/hoverable-collapse.js"></script>
    <script src="assets/js/misc.js"></script>
    <!-- endinject -->

  </body>
</html>

