<nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item <?php $test = isset($_GET['action']) ? $_GET['action'] : ''; if($test=='dashboard') echo 'active';?>">
              <a class="nav-link" href="?action=dashboard">
                <span class="menu-title">Tablero</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link collapsed" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <span class="menu-title">Registro</span>
                <i class="menu-arrow"></i>
                <i class="mdi mdi-comment-outline menu-icon"></i>
              </a>
              <div class="collapse" id="ui-basic" style="">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item <?php $test = isset($_GET['action']) ? $_GET['action'] : ''; if($test=='registerO') echo 'active';?>"> <a class="nav-link" href="?action=register&type=O">Ordinario</a></li>
                  <li class="nav-item <?php $test = isset($_GET['action']) ? $_GET['action'] : ''; if($test=='registerE') echo 'active';?>"> <a class="nav-link" href="?action=register&type=E">Extraordinario</a></li>
                </ul>
              </div>
            </li>


            <li class="nav-item <?php $test = isset($_GET['action']) ? $_GET['action'] : ''; if($test=='report') echo 'active'; ?>">
              <a class="nav-link" href="?action=report">
                <span class="menu-title">Reporte</span>
                <i class="mdi mdi-database menu-icon"></i>
              </a>
            </li>
            <?php if($test=='report') { ?>
              <li class="nav-item sidebar-actions">
              <span class="nav-link">
                <div class="border-bottom">
                  <h6 class="font-weight-normal mb-3">Filtros</h6>
                </div>
                <button class="btn btn-block btn-lg btn-gradient-primary mt-4" data-toggle="modal" data-target="#filterModal">+ Agregar Filtro</button>
                <div class="mt-4">
                  <div class="border-bottom">
                    <p class="text-secondary">Filtrado por</p>
                  </div>
                  <ul class="gradient-bullet-list mt-4">
                    <li>PROCESO</li>
                    <li>CONDUCTAS</li>
                    <li>QUEJOSO</li>
                    <li>DENUNCIADO</li>
                    <li>FECHA</li>

                  </ul>
                </div>
              </span>
            </li>
            <?php } ?>
          </ul>
        </nav>