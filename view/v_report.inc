
    <div class="container-scroller">

      <?php include 'p_navbar.inc'; ?>

      <div class="container-fluid page-body-wrapper">
        <?php include 'p_sidebar.inc'; ?>

        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-database"></i>
                </span> Reporte de Quejas y Denuncias</h3>
            </div>
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Reporte de registros almacenados</h4>

                    <table id="report" class="table table-striped">
                      <thead>
                        <tr>
                          <th> Expediente </th>
                          <th> Quejoso </th>
                          <th> Estatus </th>
                          <th> Hecho Denunciado </th>
                          <th> Fecha </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                             $rows = $auth->call("c_datos_expedientes_sp");
                             foreach ($rows as $rs){
                              echo "<tr><td>".$rs["num_expediente"]."</td><td>".$rs["nombre_solicitante"]."</td><td></td><td style='white-space: normal'>".$rs["descripcion_expediente"]."</td><td>".$rs["fecha_expediente"]."</td></tr>";
                             }
                              ?>

                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>

<script type="text/javascript" charset="utf-8" async defer>

  $(document).ready( function () {
    $('#report').DataTable({
      buttons: [
        'copy', 'excel', 'pdf', 'print'
    ],
  language: {
        "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar en resultados:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
    }
  });
} );
</script>

          </div>
          <!-- content-wrapper ends -->
          <?php include 'p_footer.inc'; ?>
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="filterModalLabel">Filtros</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="forms-sample">
                      <div class="form-group row">
                        <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Proceso</label>
                        <div class="col-sm-9">
                          <select name="" class="form-control text-dark">
                            <option value="O">ORDINARIO</option>
                            <option value="E">EXTRAORDINARIO</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Quejoso</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Nombre del Quejoso">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputEmail2" class="col-sm-3 col-form-label">Denunciado</label>
                        <div class="col-sm-9">
                          <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Nombre del Denunciado">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="exampleInputMobile" class="col-sm-3 col-form-label">Fecha de Registro</label>
                        <div class="col-sm-9">
                          <input type="date" class="form-control" id="exampleInputMobile" placeholder="dd/mm/aaaa">
                        </div>
                      </div>
                    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Filtrar</button>
      </div>
    </div>
  </div>
</div>
