<div class="container-scroller">
  <?php include 'p_navbar.inc'; ?>
  <div class="container-fluid page-body-wrapper">
    <?php include 'p_sidebar.inc'; ?>
    <div class="main-panel">
      <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
              <i class="mdi mdi-home"></i>
            </span> Tablero </h3>
            <nav aria-label="breadcrumb">
              <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                  <span></span>Actualizado al <?php echo date("d/m/Y");?> <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                </li>
              </ul>
            </nav>
          </div>
          <div class="row">
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-info card-img-holder text-white">
                <div class="card-body">
                  <img src="assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                  <h4 class="font-weight-normal mb-3">Admitidos<i class="mdi mdi-chart-line mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">1,000</h2>
                  <h6 class="card-text">de un total de 1,500 registros</h6>
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-success card-img-holder text-white">
                <div class="card-body">
                  <img src="assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                  <h4 class="font-weight-normal mb-3">Reservados <i class="mdi mdi-bookmark-outline mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">45</h2>
                  <h6 class="card-text"></h6>
                </div>
              </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
              <div class="card bg-gradient-danger card-img-holder text-white">
                <div class="card-body">
                  <img src="assets/images/dashboard/circle.svg" class="card-img-absolute" alt="circle-image" />
                  <h4 class="font-weight-normal mb-3">Desechados<i class="mdi mdi-alert mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5">10</h2>
                  <h6 class="card-text"></h6>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Próximos a vencer</h4>
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th> Expediente </th>
                          <th> Quejoso </th>
                          <th> Estatus </th>
                          <th> Fecha de Remisión </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>IEQROO/PES/001/19</td>
                          <td>Partido MORENA</td>
                          <td><label class="badge badge-gradient-success">ATENDIDO</label></td>
                          <td> 11/09/2019 </td>
                        </tr>
                        <tr>
                          <td>IEQROO/PES/001/19</td>
                          <td>Partido MORENA</td>
                          <td><label class="badge badge-gradient-warning">EN PROCESO</label></td>
                          <td> 11/09/2019 </td>
                        </tr>
                        <tr>
                          <td>IEQROO/PES/001/19</td>
                          <td>Partido MORENA</td>
                          <td><label class="badge badge-gradient-danger">VENCIDO</label></td>
                          <td> 11/09/2019 </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <?php include 'p_footer.inc'; ?>
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
