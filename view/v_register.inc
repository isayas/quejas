<style type="text/css" media="screen">
/* @extend display-flex; */
display-flex, .signup-content, .actions ul, .actions ul li a {
  display: flex;
  display: -webkit-flex; }

/* @extend list-type-ulli; */
list-type-ulli, .actions ul {
  list-style-type: none;
  margin: 0;
  padding: 0; }


h2 {
  font-size: 27px;
  color: #222;
  font-weight: 800;
  margin: 0px;
  padding-bottom: 65px; }
  h2 span {
    color: #7c7cdd; }


p {
  margin: 0px; }

p.desc {
  padding-bottom: 330px;
  color: #999;
  line-height: 1.92; }

p.title {
  font-weight: bold;
  padding-bottom: 7px; }

fieldset {
  border: none;
  padding: 0px;
  margin: 0px; }

input {
  display: block;
  width: 100%;
  border: none;
  border-bottom: 1px solid #bfbfbf;
  box-sizing: border-box;
  font-weight: bold;
  font-size: 24px;
  transition: 0.3s ease;
  -moz-transition: 0.3s ease;
  -webkit-transition: 0.3s ease;
  -o-transition: 0.3s ease;
  -ms-transition: 0.3s ease;
  padding-top: 30px;
  padding-bottom: 5px;
  -webkit-appearance: none;
  background: transparent; }
  input + label {
    position: absolute;
    transition: 0.25s ease;
    -moz-transition: 0.25s ease;
    -webkit-transition: 0.25s ease;
    -o-transition: 0.25s ease;
    -ms-transition: 0.25s ease;
    left: 0;
    top: 25px;
    font-size: 24px;
    font-weight: bold;
    color: #888; }
  input:focus + label {
    top: -7px;
    font-size: 14px;
    font-weight: 500; }
  input:valid + label {
    top: -7px;
    font-size: 14px;
    font-weight: 500; }

.form-group {
  position: relative; }

.field-icon {
  font-size: 24px;
  position: relative;
  z-index: 2;
  float: right;
  margin-top: -37px; }

.content {
  position: relative; }
  .content h3 {
    display: none; }

.step-current {
  position: absolute;
  right: 0;
  top: 0;
  text-transform: uppercase;
  font-weight: 800;
  color: #7c7cdd; }

.actions ul {
  width: 100%;
  justify-content: flex-end;
  -moz-justify-content: flex-end;
  -webkit-justify-content: flex-end;
  -o-justify-content: flex-end;
  -ms-justify-content: flex-end; }
  .actions ul .disabled {
    display: none; }
  .actions ul li {
    margin-left: 20px; }
    .actions ul li:first-child a {
      background: #fff;
      color: #999; }
    .actions ul li a {
      width: 140px;
      height: 50px;
      color: #fff;
      font-family: 'Montserrat';
      font-size: 13px;
      font-weight: bold;
      background: #7c7cdd;
      align-items: center;
      -moz-align-items: center;
      -webkit-align-items: center;
      -o-align-items: center;
      -ms-align-items: center;
      justify-content: center;
      -moz-justify-content: center;
      -webkit-justify-content: center;
      -o-justify-content: center;
      -ms-justify-content: center;
      text-decoration: none; }

.steps {
  display: none; }

@media screen and (max-width: 992px) {
  .container {
    width: calc(100% - 40px);
    max-width: 100%; }

  .signup-form {
    padding: 55px 30px; }
 }
@media screen and (max-width: 768px) {
  .steps ul, .signup-content {
    flex-direction: column;
    -moz-flex-direction: column;
    -webkit-flex-direction: column;
    -o-flex-direction: column;
    -ms-flex-direction: column; }

  .steps ul li, .signup-desc, .signup-form-conent {
    width: 100%; } }
@media screen and (max-width: 480px) {
  .actions ul li a {
    width: 100px; } }

</style>

<div class="container-scroller">

  <?php include 'p_navbar.inc'; ?>

  <div class="container-fluid page-body-wrapper">
    <?php include 'p_sidebar.inc'; ?>

    <div class="main-panel">
      <div class="content-wrapper">
        <div class="page-header">
          <h3 class="page-title">
            <span class="page-title-icon bg-gradient-primary text-white mr-2">
              <i class="mdi mdi-comment-outline"></i>
            </span> Registro de Quejas y Denuncias - Periodo <?php $test = isset($_GET['type']) ? $_GET['type'] : ''; if($test=='O') echo 'ORDINARIO'; else if($test=='E') echo 'EXTRAORDINARIO' ?></h3>

          </div>
          <div class="row">
            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">

                  <form method="POST" id="reg-form" class="signup-form" enctype="multipart/form-data">
                    <h3></h3>
                    <fieldset>
                      <span class="step-current">PASO 1 / 4 - INFORMACIÓN GENERAL</span>

                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="col-form-label">Fecha</label>
                            <input type="date" class="form-control" id="fecha" <?php $dateNow=date("Y-m-d"); echo 'value="'.$dateNow.'" min="" max="'.$dateNow.'"'; ?> required/>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="col-form-label">Hora</label>
                            <input type="time" class="form-control" id="hora" <?php $timeNow=date("H:i"); echo 'value="'.$timeNow.'"'; ?> min="09:00" max="18:00" required/>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-form-label">Lugar</label>
                            <select class="form-control text-dark" name="lugar">
                              <option selected disabled value="0">Selecionar...</option>
                              <option value= "1"> CONSEJO GENERAL</option>
                              <option value= "2"> CONSEJO DISTRITAL I</option>
                              <option value= "3"> CONSEJO DISTRITAL II</option>
                              <option value= "4"> CONSEJO DISTRITAL III</option>
                              <option value= "5"> CONSEJO DISTRITAL IV</option>
                              <option value= "6"> CONSEJO DISTRITAL V</option>
                              <option value= "7"> CONSEJO DISTRITAL VI</option>
                              <option value= "8"> CONSEJO DISTRITAL VII</option>
                              <option value= "9"> CONSEJO DISTRITAL VIII</option>
                              <option value= "10"> CONSEJO DISTRITAL IX</option>
                              <option value= "11"> CONSEJO DISTRITAL X</option>
                              <option value= "12"> CONSEJO DISTRITAL XI</option>
                              <option value= "13"> CONSEJO DISTRITAL XII</option>
                              <option value= "14"> CONSEJO DISTRITAL XIII</option>
                              <option value= "15"> CONSEJO DISTRITAL XIV</option>
                              <option value= "16"> CONSEJO DISTRITAL XV</option>
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label">Nombre del quejoso o denunciante</label>
                            <input type="text" class="form-control form-control-lg" placeholder="Nombre completo" name="nombre_quejoso" />
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="col-form-label">Nombre del denunciado o probable responsable</label>
                            <input type="text" class="form-control form-control-lg" placeholder="Nombre completo" name="nombre_denunciado" />
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-form-label">Partido Político</label>
                          <select class="form-control text-dark" name="partido">
                            <option selected disabled value="0">Selecionar...</option>
                            <option value= "1"> PAN</option>
                            <option value= "2"> PRI </option>
                            <option value= "3"> PRD </option>
                            <option value= "4"> PVEM </option>
                            <option value= "5"> MOVIMIENTO CIUDADANO </option>
                            <option value= "6"> PT </option>
                            <option value= "7"> NA </option>
                            <option value= "8"> MORENA </option>
                            <option value= "10"> ENCUENTRO SOCIAL </option>
                            <option value= "11"> JUNTOS POR MAS </option>
                            <option value= "12"> UNA NUEVA ESPERANZA </option>
                            <option value= "13"> CONFIANZA POR QUINTA ROO </option>
                            <option value= "14"> MOVIMIENTO AUTÉNTICO SOCIAL </option>
                            <option value= "O"> OTRO </option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-form-label">Medidas Cautelares</label>

                          <div class="form-check">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input"> Presenta medidas cautelares <i class="input-helper"></i></label>
                            </div>
                          </div>
                        </div>
                      </div>

                        </fieldset>

                        <h3></h3>
                        <fieldset>
                          <span class="step-current">PASO 2 / 4 - FALTA O INFRACCIÓN</span>
                          <div class="form-group">
                            <label class="col-form-label">Probable falta o infracción cometida</label>
                            <textarea class="form-control" name="falta_infraccion" rows="10"></textarea>
                          </div>
                        </fieldset>

                        <h3></h3>
                        <fieldset>
                          <span class="step-current">PASO 3 / 4 - PROCEDIMIENTO</span>
                          <div class="form-group">
                            <label for="procedimiento">Tipo de procedimiento</label>
                            <select class="form-control form-control-lg procedimiento" name="procedimiento">
                              <option value="" selected="">--SELECCIONAR--</option>
                              <?php
                              $sql = $auth->call("c_conductas_sp");
                              print_r($sql[0]["componente"]);
                              ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label for="conducta">Conducta</label>
                            <select class="form-control form-control-lg conducta" name="conducta">

                            </select>
                          </div>
                        </fieldset>

                        <h3></h3>
                        <fieldset>
                          <span class="step-current">PASO 4 / 4 - ANEXOS</span>
                          <div class="form-group">
                            <label>Adjuntar documento(s)</label>
                            <input type="file" name="docs[]" class="file-upload-default">
                            <div class="input-group col-xs-12">
                              <input type="text" class="form-control file-upload-info" disabled="" placeholder="Subir documento">
                              <span class="input-group-append">
                                <button type="button" class="btn btn-outline-danger btn-icon-text">
                                  <i class="mdi mdi-upload btn-icon-prepend"></i> SUBIR </button>

                                </span>
                              </div>
                            </div>

                          </fieldset>
                        </form>
                        <script type="text/javascript">
                          (function($) {
                            var form = $("#reg-form");
                            form.steps({
                              headerTag: "h3",
                              bodyTag: "fieldset",
                              transitionEffect: "fade",
                              labels: {
                                previous: 'Anterior',
                                next: 'Siguiente',
                                finish: 'Enviar',
                                current: ''
                              },
                              titleTemplate: '<h3 class="title">#title#</h3>',
                              onFinishing: function (event, currentIndex) {
                                var result = $('ul[aria-label=Pagination]').children().find('a');
                                $(result).each(function () {
                                  if ($(this).text() == 'Enviar') {
                                    $(this).css('display', 'none');
                                  }
                                }); return true;

                              },
                              onFinished: function(event, currentIndex) {
                                event.preventDefault();
                                $.ajax({
                                  type: form.attr('method'),
                                  url: 'control/c_registro.php',
                                  data: form.serialize(),
                                  success: function (data) {
                                    console.log('Submission was successful.');
                                    console.log(data);

                                    Swal.fire({
                                      type: 'info',
                                      title: data,
                                      text: 'Registrado con éxito!'
                                    });
                                  },
                                  error: function (data) {
                                    console.log('An error occurred.');
                                    console.log(data);
                                  },
                                });


                                var result = $('ul[aria-label=Pagination]').children().find('a');
                                $(result).each(function () {
                                  if ($(this).text() == 'Enviar') {
                                    $(this).css('display', 'flex');
                                  }
                                });
                              },
                            });

                          })(jQuery);

                          $("select.procedimiento").change(function(){
                            var selectedP = $(".procedimiento option:selected").val();
                            $.ajax({
                              type: "POST",
                              url: "control/c_conductas.php",
                              data: { conducta : selectedP }
                            }).done(function(data){
                              $(".conducta").empty().append(data);
                            });
                          });

                        </script>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- content-wrapper ends -->
              <?php include 'p_footer.inc'; ?>
            </div>
            <!-- main-panel ends -->
          </div>
          <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->

